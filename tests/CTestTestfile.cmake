# CMake generated Testfile for 
# Source directory: /Users/bjjb/code/chromaprint/tests
# Build directory: /Users/bjjb/code/chromaprint/tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(ChromaprintTests "all_tests")
subdirs(gtest_build)
